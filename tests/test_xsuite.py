from pathlib import Path
import pytest
import json
import pandas as pd

REPOSITORY_TOP_LEVEL = Path(__file__).resolve().parent.parent
REFERENCE_FILE = json.load(open(REPOSITORY_TOP_LEVEL/"reference_parameters.json"))
TWISS_DIR = REPOSITORY_TOP_LEVEL/'public'

TUNE_ACCURACY = REFERENCE_FILE['TUNE_ACCURACY']
EMITTANCE_ACCURACY = REFERENCE_FILE['EMITTANCE_ACCURACY']
BETA_ACCURACY = REFERENCE_FILE['BETA_ACCURACY']

def test_betastar(operation_mode):
    twiss=pd.read_csv(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_xsuite.csv", index_col='name')
    twiss=twiss[twiss.index.str.contains('^IP[A-G]$')] # select all IPs, ensure no other element containing IP fall into that

    for ip in twiss.index:
        assert abs(twiss.loc[ip,'betx'] - REFERENCE_FILE[operation_mode]['BETASTAR_X']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'bety'] - REFERENCE_FILE[operation_mode]['BETASTAR_Y']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'alfx']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'alfy']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'dx']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'dy']) < BETA_ACCURACY


def test_tune(operation_mode):
    twiss=pd.read_csv(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_xsuite.csv", index_col='name')

    # select last element for tune check
    assert abs(twiss.iloc[-1]['mux']%1 - REFERENCE_FILE[operation_mode]['Q1']) < TUNE_ACCURACY
    assert abs(twiss.iloc[-1]['muy']%1 - REFERENCE_FILE[operation_mode]['Q2']) < TUNE_ACCURACY


def test_emittance(operation_mode):
    emittance=pd.read_csv(TWISS_DIR/operation_mode/f'emittances_{operation_mode}_xsuite.csv', header=None, index_col=False, names=['EX', 'EY', 'EZ'])

    assert abs(emittance['EX'][0] - REFERENCE_FILE[operation_mode]['EMITTANCE_X'])/REFERENCE_FILE[operation_mode]['EMITTANCE_X'] < EMITTANCE_ACCURACY
    assert abs(emittance['EY'][0] - REFERENCE_FILE[operation_mode]['EMITTANCE_Y'])/REFERENCE_FILE[operation_mode]['EMITTANCE_Y'] < EMITTANCE_ACCURACY

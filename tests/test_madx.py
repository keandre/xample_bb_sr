from pathlib import Path
import pytest
import json
import tfs
from pandas.testing import assert_series_equal

REPOSITORY_TOP_LEVEL = Path(__file__).resolve().parent.parent
REFERENCE_FILE = json.load(open(REPOSITORY_TOP_LEVEL/"reference_parameters.json"))
TWISS_DIR = REPOSITORY_TOP_LEVEL/'public'

TUNE_ACCURACY = REFERENCE_FILE['TUNE_ACCURACY']
EMITTANCE_ACCURACY = REFERENCE_FILE['EMITTANCE_ACCURACY']
BETA_ACCURACY = REFERENCE_FILE['BETA_ACCURACY']

THIN_REL_TOLERANCE = {'t': 1.5e-2, 'z':1.5e-2}
THIN_ABS_TOLERANCE = {'t': 2e-1, 'z':1.2} # refine Z matching at FSCWL to reduce this tolerance

PLANES = ['X', 'Y', 'Z']


def test_emit(operation_mode):
    emittab = tfs.read(TWISS_DIR/operation_mode/f"emitsums_{operation_mode}.tfs")
    assert abs(emittab.iloc[0]['EX'] - REFERENCE_FILE[operation_mode]['EMITTANCE_X'])/REFERENCE_FILE[operation_mode]['EMITTANCE_X'] < EMITTANCE_ACCURACY

    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_nottapered.tfs")
    assert abs(twiss.headers['EX'] - REFERENCE_FILE[operation_mode]['EMITTANCE_X'])/REFERENCE_FILE[operation_mode]['EMITTANCE_X'] < EMITTANCE_ACCURACY
    assert abs(twiss.headers['EY'] - REFERENCE_FILE[operation_mode]['EMITTANCE_Y'])/REFERENCE_FILE[operation_mode]['EMITTANCE_Y'] < EMITTANCE_ACCURACY
    assert abs(twiss.headers['EY']/twiss.headers['EX']) - REFERENCE_FILE[operation_mode]['EMITTANCE_RATIO'] < EMITTANCE_ACCURACY
    assert abs(twiss.headers['PC'] - REFERENCE_FILE[operation_mode]['ENERGY']) < TUNE_ACCURACY


def test_tune(operation_mode):
    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_nottapered.tfs")
    assert abs(twiss.headers['Q1']%1 - REFERENCE_FILE[operation_mode]['Q1']) < TUNE_ACCURACY
    assert abs(twiss.headers['Q2']%1 - REFERENCE_FILE[operation_mode]['Q2']) < TUNE_ACCURACY


def test_betastar(operation_mode):
    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_nottapered.tfs")
    twiss=twiss[twiss['NAME'].str.contains('^IP$')] # select all IPs, ensure no other element containing IP fall into that
    for ip in twiss.index:
        assert abs(twiss.loc[ip,'BETX'] - REFERENCE_FILE[operation_mode]['BETASTAR_X']) < BETA_ACCURACY
        assert abs(twiss.loc[ip,'BETY'] - REFERENCE_FILE[operation_mode]['BETASTAR_Y']) < BETA_ACCURACY
        # assert abs(twiss.loc[ip,'ALFX']) < BETA_ACCURACY
        # assert abs(twiss.loc[ip,'ALFY']) < BETA_ACCURACY
        # assert abs(twiss.loc[ip,'DX']) < BETA_ACCURACY
        # assert abs(twiss.loc[ip,'DY']) < BETA_ACCURACY


def test_thin(operation_mode):
    # load thick and thin twiss, take all markers from thick twiss and compare optics functions
    twiss=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_nottapered.tfs")
    twiss_thin=tfs.read(TWISS_DIR/operation_mode/f"twiss_{operation_mode}_b1_thin_nottapered.tfs")

    MARKERSELECTION = '^IP|^CELL'
    twiss= twiss.loc[twiss.NAME.str.contains(MARKERSELECTION)]
    twiss_thin = twiss_thin.loc[twiss_thin.NAME.str.contains(MARKERSELECTION)]
    # thin twiss has more markers for rematching and from slicing, use only those present in thick twiss

    for marker1, marker2 in zip(twiss.index, twiss_thin.index):

        assert abs(twiss.loc[marker1,'BETX'] - twiss_thin.loc[marker2,'BETX'])/twiss.loc[marker1,'BETX'] < THIN_REL_TOLERANCE[operation_mode],\
            f"Beta_x at {twiss.loc[marker1,'NAME']}: Twiss thick: {twiss.loc[marker1,'BETX']}, Twiss thin: {twiss_thin.loc[marker2,'BETX']}"
        assert abs(twiss.loc[marker1,'BETY'] - twiss_thin.loc[marker2,'BETY'])/twiss.loc[marker1,'BETY'] < THIN_REL_TOLERANCE[operation_mode],\
            f"Beta_y at {twiss.loc[marker1,'NAME']}: Twiss thick: {twiss.loc[marker1,'BETY']}, Twiss thin: {twiss_thin.loc[marker2,'BETY']}"

        assert abs(twiss.loc[marker1,'ALFX'] - twiss_thin.loc[marker2,'ALFX']) < THIN_ABS_TOLERANCE[operation_mode],\
            f"Alpha_x at {twiss.loc[marker1,'NAME']}: Twiss thick: {twiss.loc[marker1,'ALFX']}, Twiss thin: {twiss_thin.loc[marker2,'ALFX']}"
        assert abs(twiss.loc[marker1,'ALFY'] - twiss_thin.loc[marker2,'ALFY']) < THIN_ABS_TOLERANCE[operation_mode],\
            f"Alpha_y at {twiss.loc[marker1,'NAME']}: Twiss thick: {twiss.loc[marker1,'ALFY']}, Twiss thin: {twiss_thin.loc[marker2,'ALFY']}"

        assert abs(twiss.loc[marker1,'DX'] - twiss_thin.loc[marker2,'DX']) < THIN_ABS_TOLERANCE[operation_mode],\
            f"D_x at {twiss.loc[marker1,'NAME']}: Twiss thick: {twiss.loc[marker1,'DX']}, Twiss thin: {twiss_thin.loc[marker2,'DX']}"
        assert abs(twiss.loc[marker1,'DPX'] - twiss_thin.loc[marker2,'DPX']) < THIN_ABS_TOLERANCE[operation_mode],\
            f"D'_x at {twiss.loc[marker1,'NAME']}: Twiss thick: {twiss.loc[marker1,'DPX']}, Twiss thin: {twiss_thin.loc[marker2,'DPX']}"


def test_survey(operation_mode):
    survey=tfs.read(TWISS_DIR/operation_mode/f"survey_madx_{operation_mode}_b1.tfs")

    # check that ring is closed by comparing X,Y,Z of start and endpoint
    assert_series_equal(survey[PLANES].iloc[0],
                       survey[PLANES].iloc[-1],
                       check_exact=False, atol=BETA_ACCURACY, rtol=0.01, check_names=False)

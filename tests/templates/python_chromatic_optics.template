
import json
from pathlib import Path
import numpy as np
import pandas as pd
import cpymad
from cpymad.madx import Madx


import xtrack as xt
import xpart as xp
import xobjects as xo

DELTAS = np.linspace(-0.02, 0.02, 21)
IP_COLUMNS = ['QX', 'QY', 'BETX', 'BETY', 'DX', 'X', 'Y']
CS_COLUMNS = ['BETX', 'BETY','ALFX', 'ALFY','MUX', 'MUY', 'DX', 'X']

def check_thick_madx():
    with Madx() as mad:
        mad.option()
        mad.call("../../lattices/{{operation_mode}}/fccee_{{operation_mode}}.seq")
        mad.beam(particle='electron',
        npart={{reference['BUNCH_POPULATION']}},
        kbunch={{reference['BUNCHES']}},
        pc={{reference['ENERGY']}},
        radiate=False,
        bv=1,
        ex={{reference['EMITTANCE_X']}},
        ey={{reference['EMITTANCE_Y']}})
        mad.use('FCCEE_P_RING')
        mad.input('RF_ON=0;')

        chrom_optics = pd.DataFrame(columns=IP_COLUMNS)
        for delta in DELTAS:
            try:
                twiss_df = mad.twiss(sequence='FCCEE_P_RING', deltap=f'{delta}', chrom=True).dframe()
                chrom_optics.loc[delta, IP_COLUMNS] = (
                    mad.table.summ['Q1'][0],
                    mad.table.summ['Q2'][0],
                    twiss_df.loc['#s', 'betx'],
                    twiss_df.loc['#s', 'bety'],
                    twiss_df.loc['#s', 'dx'],
                    twiss_df.loc['#s', 'x'],
                    twiss_df.loc['#s', 'y'],
                )
            except cpymad.madx.TwissFailed:
                chrom_optics.loc[delta, IP_COLUMNS] = (
                    float('nan'),
                    float('nan'),
                    float('nan'),
                    float('nan'),
                    float('nan'),
                    float('nan'),
                    float('nan'),
                )

        chrom_optics.to_csv('chromatic_properties_{{operation_mode}}.csv', index=True, index_label='DELTAP')


def check_thin_madx():
    with Madx() as mad:
        mad.option()
        mad.call("fcc_ee_{{operation_mode}}_b1_thin_rf.seq")
        mad.beam(particle='electron',
        npart={{reference['BUNCH_POPULATION']}},
        kbunch={{reference['BUNCHES']}},
        pc={{reference['ENERGY']}},
        radiate=False,
        bv=1,
        ex={{reference['EMITTANCE_X']}},
        ey={{reference['EMITTANCE_Y']}})
        mad.use('FCCEE_P_RING')
        mad.input('RF_ON=0;')

        chrom_optics = pd.DataFrame(columns=IP_COLUMNS)
        for delta in DELTAS:
            try:
                twiss_df = mad.twiss(sequence='FCCEE_P_RING', deltap=f'{delta}', chrom=True).dframe()
                chrom_optics.loc[delta, IP_COLUMNS] = (
                    mad.table.summ['Q1'][0],
                    mad.table.summ['Q2'][0],
                    twiss_df.loc['#s', 'betx'],
                    twiss_df.loc['#s', 'bety'],
                    twiss_df.loc['#s', 'dx'],
                    twiss_df.loc['#s', 'x'],
                    twiss_df.loc['#s', 'y'],
                )
            except cpymad.madx.TwissFailed:
                chrom_optics.loc[delta, IP_COLUMNS] = (
                    float('nan'),
                    float('nan'),
                    float('nan'),
                    float('nan'),
                    float('nan'),
                    float('nan'),
                    float('nan'),
                )

        chrom_optics.to_csv('chromatic_properties_thin_{{operation_mode}}.csv', index=True, index_label='DELTAP')


def check_thin_xsuite():
    filename = Path("fcc_ee_{{operation_mode}}_b1_thin_rf.json")
    # Load json
    with open(filename, 'r', encoding='utf-8') as fid:
        loaded_dct = json.load(fid)
    line = xt.Line.from_dict(loaded_dct)
    # line.cycle(name_first_element='frf.1')
    context = xo.ContextCpu()
    ref_particle = xp.Particles(mass0=xp.ELECTRON_MASS_EV, q0=1, p0c={{reference['ENERGY']}}*10**9, x=0, y=0)
    line.particle_ref = ref_particle
    line.build_tracker(_context=context)
    line.config.XTRACK_USE_EXACT_DRIFTS = True
    line.configure_radiation(model='mean')
    line.compensate_radiation_energy_loss()
    twiss = line.twiss(eneloss_and_damping=True, radiation_method='full', method="6d")

    chrom_optics = pd.DataFrame(columns=IP_COLUMNS)
    for delta in DELTAS:
        try:
            twiss = line.twiss(eneloss_and_damping=True, method='4d', delta0=delta)
            chrom_optics.loc[delta, IP_COLUMNS] = (
                twiss.qx,
                twiss.qy,
                twiss['betx'][0],
                twiss['bety'][0],
                twiss['dx'][0],
                twiss['x'][0],
                twiss['y'][0],
            )
        except (xt.twiss.ClosedOrbitSearchError, ValueError, AssertionError):
            chrom_optics.loc[delta, IP_COLUMNS] = (
                float('nan'),
                float('nan'),
                float('nan'),
                float('nan'),
                float('nan'),
                float('nan'),
                float('nan'),
            )

    chrom_optics.to_csv('chromatic_properties_thin_xsuite_{{operation_mode}}.csv', index=True, index_label='DELTAP')


def check_pathlength():
    with Madx() as mad:
        mad.option()
        mad.call("fcc_ee_{{operation_mode}}_b1_thin_rf.seq")
        mad.beam(particle='electron',
        npart={{reference['BUNCH_POPULATION']}},
        kbunch={{reference['BUNCHES']}},
        pc={{reference['ENERGY']}},
        radiate=False,
        bv=1,
        ex={{reference['EMITTANCE_X']}},
        ey={{reference['EMITTANCE_Y']}})
        mad.use('FCCEE_P_RING')
        mad.input('RF_ON=0;')
        mad.input('CALL, FILE="../../toolkit/install_matching_markers.madx";')
        mad.twiss()
        sigma_x = np.sqrt({{reference['EMITTANCE_X']}}*{{reference['BETASTAR_X']}})
        sigma_y = np.sqrt({{reference['EMITTANCE_Y']}}*{{reference['BETASTAR_Y']}})
        x_range = np.linspace(-15*sigma_x, 15*sigma_x, 101, endpoint=True)
        y_range = np.linspace(-15*sigma_y, 15*sigma_y, 101, endpoint=True)
        mad.use('FCCEE_P_RING')
        mad.command.track(onetable=True)
        for x in x_range:
            mad.command.start(
                X=x,
                PX=0.0,
                Y=0.0,
                PY=0.0,
                T=0.0,
                PT=0.0)
        for y in y_range:
            mad.command.start(
                X=0.0,
                PX=0.0,
                Y=y,
                PY=0.0,
                T=0.0,
                PT=0.0)
        mad.command.run(turns=1)
        mad.command.endtrack()

        df = mad.table.trackone.dframe().copy()
        result_df = pd.DataFrame(data={
            'X': df.loc[df['turn']==0]['x'].to_numpy(),
            'Y': df.loc[df['turn']==0]['y'].to_numpy(),
            'T': df.loc[df['turn']==1]['t'].to_numpy(),
        })
        result_df.to_csv('path_length_vs_delta_{{operation_mode}}.csv')


def check_chromatic_propagation_between_CS():
    with Madx() as mad:
        mad.option()
        mad.call("../../lattices/{{operation_mode}}/fccee_{{operation_mode}}.seq")
        mad.beam(particle='electron',
        npart={{reference['BUNCH_POPULATION']}},
        kbunch={{reference['BUNCHES']}},
        pc={{reference['ENERGY']}},
        radiate=False,
        bv=1,
        ex={{reference['EMITTANCE_X']}},
        ey={{reference['EMITTANCE_Y']}})
        mad.use('FCCEE_P_RING')
        mad.input('RF_ON=0;')
        mad.input('CALL, FILE="../../toolkit/install_matching_markers.madx";')
        mad.input('SAVEBETA, LABEL=B.CSL, PLACE=CCS_XL.D, SEQUENCE=FCCEE_P_RING;')
        mad.twiss()
        mad.input("""
        SEQEDIT, SEQUENCE=FCCEE_P_RING;
            FLATTEN;
            CYCLE, START=CCS_XL.D;
            FLATTEN;
        ENDEDIT;""")
        mad.use('FCCEE_P_RING')

        chrom_optics = pd.DataFrame(columns=CS_COLUMNS)
        for delta in DELTAS:
            try:
                twiss_df = mad.twiss(sequence='FCCEE_P_RING', deltap=f'{delta}', chrom=True, beta0='B.CSL').dframe()
                chrom_optics.loc[delta, CS_COLUMNS] = (
                    twiss_df.loc['ccs_xr.d', 'betx'],
                    twiss_df.loc['ccs_xr.d', 'bety'],
                    twiss_df.loc['ccs_xr.d', 'alfx'],
                    twiss_df.loc['ccs_xr.d', 'alfy'],
                    twiss_df.loc['ccs_xr.d', 'mux'],
                    twiss_df.loc['ccs_xr.d', 'muy'],
                    twiss_df.loc['ccs_xr.d', 'dx'],
                    twiss_df.loc['ccs_xr.d', 'x'],
                )
            except cpymad.madx.TwissFailed:
                chrom_optics.loc[delta, CS_COLUMNS] = (
                    float('nan'),
                    float('nan'),
                    float('nan'),
                    float('nan'),
                    float('nan'),
                    float('nan'),
                    float('nan'),
                    float('nan'),
                )

        chrom_optics.to_csv('chromatic_propagation_{{operation_mode}}.csv', index=True, index_label='DELTAP')


# Script Mode ------------------------------------------------------------------

if __name__ == "__main__":
    check_thick_madx()
    # check_thin_madx()
    check_thin_xsuite()
    check_chromatic_propagation_between_CS()
    # check_pathlength()
